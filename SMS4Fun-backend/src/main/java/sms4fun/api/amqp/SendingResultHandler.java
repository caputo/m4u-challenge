package sms4fun.api.amqp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sms4fun.api.domain.Handout;
import sms4fun.api.domain.HandoutRepository;

@Component
public class SendingResultHandler {
    @Autowired
    private HandoutRepository handoutRepository;

    private static final Logger log = LoggerFactory
            .getLogger(SendingResultHandler.class);

    public void handleMessage(SendingResultMessage sendingResultMessage) {
        final String token = sendingResultMessage.getToken();
        log.info("Received summary: " + token);

        final List<Handout> handouts = handoutRepository.findByToken(token);
        if (handouts.size() == 0) {
            log.info("No handout of token number: " + token + " found.");
        } else {
            for (Handout handout : handouts) {
                handout.setToken(sendingResultMessage.getToken());
                handout.setStatus(sendingResultMessage.getStatus());
                handoutRepository.save(handouts);
                log.info("updated handout: " + token);
            }
        }
    }
}
