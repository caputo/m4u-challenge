package sms4fun.api.amqp;

public class TaskMessage {

    private String token;
    private String apart;
    private String bpart;
    private String body;
    private String status;

    public String getApart() {
        return apart;
    }

    public void setApart(String apart) {
        this.apart = apart;
    }

    public String getBpart() {
        return bpart;
    }

    public void setBpart(String bpart) {
        this.bpart = bpart;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
