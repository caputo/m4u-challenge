package sms4fun.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


public class DateUtil {

    public static LocalDate getLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDate getTodayDate() {
        return getLocalDate(new Date(new Date().getTime()));
    }

    // Todo - Corrigir D + 1 recebido  “new Date().getTime() + (1000 * 60 * 60 * 24))”
    public static LocalDate fixDate(Date date) {
        return getLocalDate(new Date(date.getTime() + (1000 * 60 * 60 * 24)));
    }


}
