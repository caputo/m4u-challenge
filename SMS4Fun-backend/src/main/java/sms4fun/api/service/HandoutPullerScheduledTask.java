package sms4fun.api.service;

import java.util.List;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sms4fun.api.amqp.TaskMessage;
import sms4fun.api.amqp.TaskProducer;
import sms4fun.api.domain.Handout;
import sms4fun.api.domain.HandoutRepository;

import static sms4fun.api.DateUtil.getTodayDate;
import static sms4fun.api.DateUtil.fixDate;


@Component
public class HandoutPullerScheduledTask {

    private static final Logger log = LoggerFactory
            .getLogger(HandoutPullerScheduledTask.class);

    @Autowired
    private TaskProducer taskProducer;

    @Value("${sms4fun.scheduledJob.enabled:false}")
    private boolean scheduledJobEnabled;

    @Autowired
    private HandoutRepository handoutRepository;

    @Scheduled(fixedRate = 30000)  // every 30 seconds
    public void sendHandout() {
        if (!scheduledJobEnabled) {
            return;
        }

        final List<Handout> handouts = handoutRepository.findAllPedingHandouts();

        final TaskMessage taskMessage = new TaskMessage();

        for (Handout handout : handouts) {
            if (handout.getStatus().equals("Peding")) {
                LocalDate todayDate = getTodayDate();
                LocalDate scheduleDate = fixDate(handout.getSchedule() );
                LocalDate expirationDate = fixDate(handout.getExpirationDate());

                if (todayDate.equals(expirationDate) || todayDate.equals(scheduleDate)) {
                    log.info("schedule:" + handout);
                    taskMessage.setToken(handout.getToken());
                    taskMessage.setApart(handout.getApart());
                    taskMessage.setBpart(handout.getBpart());
                    taskMessage.setBody(handout.getBody());
                    taskMessage.setStatus(handout.getStatus());
                    taskProducer.sendNewTask(taskMessage);
                }
            }
        }
        log.info("TODO");
    }

}
