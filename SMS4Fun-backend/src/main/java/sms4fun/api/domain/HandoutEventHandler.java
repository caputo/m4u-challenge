package sms4fun.api.domain;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import sms4fun.api.amqp.TaskMessage;
import sms4fun.api.amqp.TaskProducer;

import static sms4fun.api.DateUtil.getLocalDate;
import static sms4fun.api.DateUtil.getTodayDate;


@RepositoryEventHandler(Handout.class)
public class HandoutEventHandler {
    @Autowired
    private TaskProducer taskProducer;

    @HandleBeforeCreate
    public void handleHandoutCreate(Handout handout) {
        handout.setCreated(new Date());
        handout.setToken(handout.getToken().trim());
        handout.setApart(handout.getApart().trim());
        handout.setBpart(handout.getBpart().trim());
        handout.setBody(handout.getBody());
        handout.setStatus(handout.getStatus().trim());
    }

    @HandleAfterCreate
    public void handleAfterHandoutCreate(Handout handout) {
        LocalDate todayDate = getTodayDate();
        LocalDate scheduleDate = getLocalDate(handout.getSchedule());
        LocalDate expirationDate = getLocalDate(handout.getExpirationDate());
        // Todo - Melhorar validação de datas “data de validade”

        if (todayDate.equals(expirationDate) || todayDate.equals(scheduleDate)) {
            final TaskMessage taskMessage = new TaskMessage();
            taskMessage.setToken(handout.getToken());
            taskMessage.setApart(handout.getApart());
            taskMessage.setBpart(handout.getBpart());
            taskMessage.setBody(handout.getBody());
            taskMessage.setStatus(handout.getStatus());
            taskProducer.sendNewTask(taskMessage);
        }
    }


}
