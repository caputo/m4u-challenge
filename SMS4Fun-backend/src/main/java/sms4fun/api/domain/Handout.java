package sms4fun.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Handout {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private String apart;

    @Column(nullable = false)
    private String bpart;

    @Column(nullable = false)
    private String status;

    @Column(columnDefinition = "Text")
    private String body;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date created;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date schedule;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date expirationDate;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApart() {
        return apart;
    }

    public void setApart(String apart) {
        this.apart = apart;
    }

    public String getBpart() {
        return bpart;
    }

    public void setBpart(String bpart) {
        this.bpart = bpart;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getSchedule() {
        return schedule;
    }

    public void setSchedule(Date schedule) {
        this.schedule = schedule;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

}
