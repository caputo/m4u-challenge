package sms4fun.api.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource
@Transactional(readOnly = true)
public interface HandoutRepository extends CrudRepository<Handout, Long> {
    @RestResource(path = "token")
    List<Handout> findByToken(String token);

    @Query("select h from Handout h where h.status = 'Peding' ")
    List<Handout> findAllPedingHandouts();
}

