// The handout model
function Handout(selfHref, token, apart, bpart, body, status, created, schedule, expirationDate) {
    var self = this;
    self.selfHref = selfHref;
    self.token = ko.observable(token);
    self.apart = ko.observable(apart);
    self.bpart = ko.observable(bpart);
    self.body = ko.observable(body);
    self.status = ko.observable(status);
    self.created = created;
    self.schedule = schedule;
    self.expirationDate = expirationDate;
}

// The handout view model
function HandoutViewModel() {
    var self = this;
    self.token = ko.observable();
    self.newApart = ko.observable();
    self.newBpart = ko.observable();
    self.newBody = ko.observable();
    self.newStatus = ko.observable();
    self.newSchedule = ko.observable();
    self.newExpirationDate = ko.observable();
    self.handouts = ko.observableArray([]);

    // add handout: send POST to handouts resource
    self.addHandout = function () {

        // a little bit of pre-processing of user entered apart and note
        var newApart = self.newApart();
        if (typeof newApart == "undefined" || newApart == "") {
            alert("Apart MSISDN required");
            return;
        }

        var newBpart = self.newBpart();
        if (typeof newBpart == "undefined" || newBpart == "") {
            alert("Bpart MSISDN required");
            return;
        }

        var newBody = self.newBody();
        if (typeof newBody == "undefined" || newBody == "") {
            alert("Message required");
            return;
        }

        var newStatus = self.newStatus();
        if (typeof newStatus == "undefined" || newStatus == "") {
            newStatus = 'Peding'
        }

        var newExpirationDate = self.newExpirationDate();
        if (typeof newExpirationDate == "undefined" || newExpirationDate == "") {
            alert("newExpirationDate");
            return;
        }
        var newSchedule = self.newSchedule();
        if (typeof newSchedule == "undefined" || newSchedule == "") {
            newSchedule = newExpirationDate
        }

        var newToken = md5(self.newApart() + self.newBpart() + self.newBody() + self.newExpirationDate());

        // make POST request
        $.ajax("http://localhost:8080/handouts", {
            data: '{"token": "' + newToken +
            ' ","apart": "' + newApart +
            ' ", "bpart": "' + newBpart +
            ' ", "body": "' + newBody +
            ' ", "status": "' + newStatus +
            ' ", "schedule": "' + newSchedule +
            ' ", "expirationDate": "' + newExpirationDate + '"}',
            type: "post",
            contentType: "application/json",
            success: function (allData) {
                self.loadHandouts();
                self.token(newToken);
                self.newApart("");
                self.newBpart("");
                self.newStatus("Peding");
                self.newSchedule(newSchedule);
                self.newExpirationDate(newExpirationDate);
                self.newBody("");
            }
        });
    };


    // delete handout: send DELETE to handouts resource
    self.deleteHandout = function (handout) {
        $.ajax(handout.selfHref, {
            type: "delete",
            success: function (allData) {
                self.loadHandouts();
            }
        });
    };

    // load handouts from server: GET on handouts resource
    self.loadHandouts = function () {
        $.ajax("http://localhost:8080/handouts", {
            type: "get",
            success: function (allData) {
                var json = ko.toJSON(allData);
                var parsed = JSON.parse(json);
                if (parsed._embedded) {
                    var parsedHandouts = parsed._embedded.handouts;
                    var mappedHandouts = $.map(parsedHandouts, function (handout) {
                        return new Handout(handout._links.self.href, handout.token, handout.apart, handout.bpart, handout.body, handout.status, handout.created, handout.schedule, handout.expirationDate)
                    });
                    self.handouts(mappedHandouts);
                }
            }
        });
    };

    // Load initial data
    self.loadHandouts();
}

// Activates knockout.js
var handoutViewModel = new HandoutViewModel();
window.setInterval(handoutViewModel.loadHandouts, 1000);
ko.applyBindings(handoutViewModel);


