import signal
import logging

from datetime import datetime
from time import sleep

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

class TimedOutExc(Exception):
    """
    Raised when a timeout happens
    """


def timeout(timeout=3000):
    def decorate(f):
        def handler(signum, frame):
            raise TimedOutExc()

        def new_f(*args, **kwargs):
            old_handler = signal.signal(signal.SIGALRM, handler)
            signal.alarm(timeout)

            result = f(*args, **kwargs)

            signal.signal(signal.SIGALRM, old_handler)
            signal.alarm(0)

            return result

        new_f.func_name = f.__class__
        return new_f

    return decorate


class ProvidingResult:
    def __init__(self):
        self.token = None
        self.apart = None
        self.bpart = None
        self.body = None
        self.status = 'Done'


class Provider:
    @timeout(3000)
    def providing_result(self, token, apart, bpart, body, status):
        try:

            providing_result = ProvidingResult()
            providing_result.token = token
            providing_result.apart = apart
            providing_result.bpart = bpart
            providing_result.body = body
            providing_result.status = status

            now = "{0:%Y-%m-%d %H:%M:%S} ".format(datetime.now())
            logging.info("{} INFO -- apart:{}, bpart:{}, status: Done".format(now, apart, bpart))

            sleep(10) # Force Sleep
            return providing_result

        except TimedOutExc:
            return None
