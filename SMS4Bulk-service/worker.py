import json
from time import sleep

import pika
import logging
from provider import Provider

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

credentials = pika.PlainCredentials("guest", "guest")

# TODO: recuperar host (host='rabbit') de aquivo de configuração ou variável de ambiente.

parameters = pika.ConnectionParameters(host='rabbit', credentials=credentials)

connected = False
while not connected:
    try:
        sleep(10)  # Force Sleep
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        tasks_queue = channel.queue_declare(queue='tasks.queue', durable=True)
        scraping_result_queue = channel.queue_declare(queue='sendingresult.queue', durable=True)
        connected = True
        logging.info('connected')
    except:
        pass #Do nothing, just try again


logging.info('[*] Waiting for tasks. To exit press CTRL+C')


def publish_result(scraping_result):
    j = json.dumps(scraping_result.__dict__)
    logging.info("result: {}".format(j))
    properties = pika.BasicProperties(content_type="application/json")
    channel.basic_publish(exchange='', routing_key='sendingresult.queue', body=j, properties=properties)


def callback(ch, method, properties, req):
    token = json.loads(req)['token']
    apart = json.loads(req)['apart']
    bpart = json.loads(req)['bpart']
    body = json.loads(req)['body']
    logging.info(json.loads(req)['status'])
    if json.loads(req)['status'] == "Peding":
        status = "Done"
    else:
        status = "Error"
    logging.info(status)
    provider = Provider()
    result = provider.providing_result(token.strip(), apart.strip(), bpart.strip(), body.strip(), status.strip())
    publish_result(result)

channel.basic_consume(callback, queue='tasks.queue', no_ack=True)
channel.start_consuming()
