# m4u-challenge

## SMS4Fun

SMS4Fun é uma solução híbrida para transmissão de mensagens SMS em massa via rede GSM, provendo a integração com gateway SMS de provedores de serviços SMSC que garantem a entrega de mensagens para números de telefone celular em qualquer lugar do mundo.

---
![alt tag](./SMS4Fun_Architecture.png)
---


**SMS4Bulk-service**

SMS4Bulk-service é um gateway SMS que atua como um relé entre os operadores de rede móvel e um provedor de serviços de aplicativos sem fio. 

Este gateway permite que o tráfego SMS seja distribuído através de uma conexão direta com o SMSC (Short Message Service Center) de uma operadora de rede móvel e, em seguida, para o número de telefone móvel de um destinatário.


**SMS4Fun-backend**

O SMS4Fun-backend atua como um Middleware responsável pelo gerenciamento das mensagens SMS, provendo assim, o agendamento das tarefas de envio e garantindo a entrega das mensagens para o gateway SMS.


**SMS4Fun-frontend**

SMS4Fun-frontend é uma interface amigável que permite ao usuário final uma visualização simples e maior controle das funcionalidades providas pelo SMS4Fun-backend.


### Dependências

**make**

```bash
→ make --version
GNU Make 3.81
```

**java**

```bash
→ java -version
java version "1.8.0_121"
```

**mvn**

```bash
→ mvn --version
Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T14:41:47-02:00)
```

**python**

```bash
→ python --version
Python 2.7.13
```

**virtualenv**

```bash
→ virtualenv --version
15.1.0
```

**git**

```bash
→ git --version
git version 2.11.0 (Apple Git-81)
```

**docker**

```bash
→ docker version
Client:
 Version:      17.03.1-ce
 API version:  1.27
 Go version:   go1.7.5
 Git commit:   c6d412e
 Built:        Tue Mar 28 00:40:02 2017
 OS/Arch:      darwin/amd64
 
Server:
 Version:      17.03.1-ce
 API version:  1.27 (minimum version 1.12)
 Go version:   go1.7.5
 Git commit:   c6d412e
 Built:        Fri Mar 24 00:00:50 2017
 OS/Arch:      linux/amd64
 Experimental: true
```

**docker-compose**

```bash
→ docker-compose version
docker-compose version 1.12.0, build b31ff33
docker-py version: 2.2.1
CPython version: 3.6.1
OpenSSL version: OpenSSL 1.0.2k  26 Jan 2017
```

##**UP!**

```bash
○ → git clone git@bitbucket.org:caputo/m4u-challenge.git
○ → cd m4u-challenge
○ → make
.
.
.
Successfully built cf0d8dbfa447
[INFO] Built sms4fun/sms4fun-backend
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 20.879 s
[INFO] Finished at: 2017-04-27T23:40:53-03:00
[INFO] Final Memory: 50M/414M
[INFO] ------------------------------------------------------------------------
Done!!!
rmi none
Go Go Go...
docker-compose logs -f
appending output to nohup.out
```


