NAME=$(shell basename $(PWD))

RELEASE_SUPPORT := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/.make-release-support
IMAGE=$(shell tr '[:upper:]' '[:lower:]' <<< $(NAME))

VERSION=$(shell . $(RELEASE_SUPPORT) ; getVersion)
TAG=$(shell . $(RELEASE_SUPPORT); getTag)

SHELL=/bin/bash

.PHONY: all

all: clean build rmi-none container-run

docker-all: pre-build docker-build post-build build release patch-release minor-release major-release tag check-status check-release showver \
	push do-push post-push

build: pre-build docker-build post-build

pre-build:
	-@echo Pull
	-@docker pull rabbitmq:3

post-build:
	-@echo "Done!!!"

post-push:
	-@echo .

container-run:
	-@echo "Go Go Go..."
	-@nohup docker-compose up -d &
	-@echo "docker-compose logs -f"

.release:
	-@echo "release=0.0.0" > .release
	-@echo "tag=$(NAME)-0.0.0" >> .release
	-@echo INFO: .release created
	-@cat .release

release: check-status check-release all push

docker-build:
	-@echo build
	-@docker build -t sms4fun/sms4bulk-service:latest SMS4Bulk-service/. ; \
	 docker build -t sms4fun/sms4fun-frontend:latest SMS4Fun-frontend/. ; \
     mvn clean package docker:build -f SMS4Fun-backend/pom.xml



-f:
push: do-push post-push

do-push:
	@echo .

snapshot: build push

showver: .release
	-@. $(RELEASE_SUPPORT); getVersion

tag-patch-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextPatchLevel)
tag-patch-release: .release tag

tag-minor-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextMinorLevel)
tag-minor-release: .release tag

tag-major-release: VERSION := $(shell . $(RELEASE_SUPPORT); nextMajorLevel)
tag-major-release: .release tag

patch-release: tag-patch-release release
	-@echo $(VERSION)

minor-release: tag-minor-release release
	-@echo $(VERSION)

major-release: tag-major-release release
	-@echo $(VERSION)


tag: TAG=$(shell . $(RELEASE_SUPPORT); getTag $(VERSION))
tag: check-status
	-@. $(RELEASE_SUPPORT) ; ! tagExists $(TAG) || (echo "ERROR: tag $(TAG) for version $(VERSION) already tagged in git" >&2 && exit 1) ;
	-@. $(RELEASE_SUPPORT) ; setRelease $(VERSION)
	-@git add .release
	-@git commit -m "bumped to version $(VERSION)" ;
	-@git tag $(TAG) ;
	-@[ -n "$(shell git remote -v)" ] && git push --tags

check-status:
	-@. $(RELEASE_SUPPORT) ; ! hasChanges || (echo "ERROR: there are still outstanding changes" >&2 && exit 1) ;

check-release: .release
	-@. $(RELEASE_SUPPORT) ; tagExists $(TAG) || (echo "ERROR: version not yet tagged in git. make [minor,major,patch]-release." >&2 && exit 1) ;
	-@. $(RELEASE_SUPPORT) ; ! differsFromRelease $(TAG) || (echo "ERROR: current directory differs from tagged $(TAG). make [minor,major,patch]-release." ; exit 1)

clean:
	-@echo kill
	-@docker kill m4uchallenge_web_1 m4uchallenge_api_1 m4uchallenge_worker_1 m4uchallenge_rabbit_1
	-@echo rm
	-@docker rm -f m4uchallenge_web_1 m4uchallenge_api_1 m4uchallenge_worker_1 m4uchallenge_rabbit_1
	-@echo rmi
	-@docker rmi -f sms4fun/sms4fun-backend sms4fun/sms4fun-frontend sms4fun/sms4bulk-service

rmi: rmi-none
	-@echo rmi
	-@docker rmi -f sms4fun/sms4fun-backend sms4fun/sms4fun-frontend sms4fun/sms4bulk-service

rmi-none:
	-@echo rmi none
	-@docker images | grep "^<none>" | awk  '{print $$3}' | xargs docker rmi -f

